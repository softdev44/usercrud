/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Orders {

    private int id;
    private Date date;
    private double total;
    private int qty;
    private ArrayList<OrderDetail> orderDetail;

    public Orders(int id, Date date, double tatal, int qty, ArrayList<OrderDetail> orderDetail) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.qty = qty;
        this.orderDetail = orderDetail;
    }

    public Orders() {
        this.id = -1;
        orderDetail = new ArrayList<>();
        qty = 0;
        total = 0;
        //date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double tatal) {
        this.total = total;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", tatal=" + total + ", qty=" + qty +  '}';
    }

    public void addOrderDetail(OrderDetail orderDetail) {
        this.orderDetail.add(orderDetail);
        total = total + orderDetail.getTotal();
        qty = qty + orderDetail.getQty();
    }
    
    //overloading
    public void addOrderDetail(Product product,String productName,double productPrice,int qty) {
        OrderDetail orderDetail = new OrderDetail(product,productName,productPrice,qty,this);
        this.addOrderDetail(orderDetail);
    }
    
     //overloading
    public void addOrderDetail(Product product,int qty) {
        OrderDetail orderDetail = new OrderDetail(product,product.getName(),product.getPrice(),qty,this);
        this.addOrderDetail(orderDetail);
    }

    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("order_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getDouble("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-mm-dd HH:mm:ss");
            order.setDate((Date) sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
}

